import { Component, OnInit, Inject } from '@angular/core';
import { WINDOW } from '@ng-toolkit/universal';
declare let $: any;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(@Inject(WINDOW) private window: Window, ) { }

  ngOnInit() {
    // Go to Top
    $(function(){
      //Scroll event
      $(this.window).on('scroll', function(){
          var scrolled = $(this.window).scrollTop();
          if (scrolled > 300) $('.go-top').fadeIn('slow');
          if (scrolled < 300) $('.go-top').fadeOut('slow');
      });  
      //Click event
      $('.go-top').on('click', function() {
          $("html, body").animate({ scrollTop: "0" },  500);
      });
    });
  }

}
