import { Component, OnInit } from '@angular/core';
import {Observable, Subject, Subscription, of} from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  subscription$: Subscription;
  tileData: any[];
  observableData: string;

  constructor() {
    this.tileData = require('../../mocks/rewards-list.json');
   }

  ngOnInit() {
    this.subscription$ = of('This is an observable string.').subscribe(
      data => {
        this.observableData = data;
      }
    );
    this.subscription$.unsubscribe();
  }

  onChange(event: string) {
    console.log(event)
  }

}
