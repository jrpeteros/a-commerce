import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tile-boxes',
  templateUrl: './tile-boxes.component.html',
  styleUrls: ['./tile-boxes.component.scss']
})
export class TileBoxesComponent implements OnInit {
  @Input() tileData: any = [];

  constructor() { }

  ngOnInit() {
    console.log(this.tileData)
  }

}
