import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TileBoxesComponent } from './tile-boxes.component';

describe('TileBoxesComponent', () => {
  let component: TileBoxesComponent;
  let fixture: ComponentFixture<TileBoxesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TileBoxesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TileBoxesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
