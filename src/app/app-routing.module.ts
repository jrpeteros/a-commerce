import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { TileBoxesComponent } from './common/tile-boxes/tile-boxes.component';
import { NotFoundComponent } from './components/not-found/not-found.component';


const routes: Routes = [
  { 
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '/rewards'
      },
      {
        path: '',
        component: TileBoxesComponent,
        outlet: 'custom-header'
      },
    ]
  },
  { path: 'rewards', component: HomeComponent },
  { 
    path: '**',
    component: NotFoundComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
