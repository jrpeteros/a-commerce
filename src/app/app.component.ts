import { Component, OnInit, OnDestroy, Inject, PLATFORM_ID } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy, isPlatformBrowser, DOCUMENT } from '@angular/common';
import { NgwWowService } from 'ngx-wow';
import { Subscription } from 'rxjs';
import { WINDOW } from '@ng-toolkit/universal';
import { environment } from '../environments/environment';
declare let $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [
    Location, {
      provide: LocationStrategy,
      useClass: PathLocationStrategy
    }
  ]
})
export class AppComponent implements OnInit {
  private wowSubscription: Subscription;
  location: any;

  constructor(@Inject(PLATFORM_ID) private platformId: any, @Inject(DOCUMENT) private document: any, @Inject(WINDOW) private window: Window, 
    private router: Router, 
    location: Location, 
    private wowService: NgwWowService
    ){
    this.wowService.init(); 
    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationStart) { 
        $('.preloader').fadeIn();
      }
      if (ev instanceof NavigationEnd) { 
        this.location = location.path();
        $('.preloader').fadeOut('slow');
      }
    });
  }

  ngOnInit() {
    // you can subscribe to WOW observable to react when an element is revealed
    this.wowSubscription = this.wowService.itemRevealed$.subscribe(
      (item:HTMLElement) => {
        // 
      });

    // Preloader
    $(this.window).on('load', function() {
      $('.preloader').fadeOut();
    });

    if (!isPlatformBrowser(this.platformId)) {
      return;
    }
  }

  ngOnDestroy() {
    // unsubscribe (if necessary) to WOW observable to prevent memory leaks
    this.wowSubscription.unsubscribe();
  }
}
