# A-Commerce / Astro-Commerce

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.8.

# DEMO via AWS S3 bucket static website
http://jose-portfolios.s3-website-us-east-1.amazonaws.com


**Software Installed Prerequisites:**

1. [NPM](https://www.npmjs.com/get-npm)
2. [angular-cli](https://angular.io/cli)
3. ng-toolkit/serverless --provider aws

### Running the app

1. Run the command: `npm start` in your terminal. If you see the below after everything, it means processing is completed & the app is running:

	```
	5 unchanged chunks
	chunk {main} main.js, main.js.map (main) 142 kB [initial] [rendered]
	Time: 634ms
	ℹ ｢wdm｣: Compiled successfully.
	```

**Knowledge Prerequisites:**

1. Knowledge of `HTML`, `CSS` & `Javascript`
2. Knowledge of `Angular5+`
2. Knowledge of `Jasmine` e.g Testbed
3. Knowledge of [SCSS](https://sass-lang.com/) (**NOT** SASS)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

**To have a covearge folder for istanbul report:**
1. Run `ng test --code-coverage`
2. Go to Coverage Folder, Copy and Paste index.html to a browser

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).


#### Other Libraries

This refers to libraries that we have to download & add manually rather than use npm or the like for various reasons:

1. [Bootstrap](https://getbootstrap.com/)
2. [Angular feeather](https://www.npmjs.com/package/angular-feather/) 
3. Flat-icon, and animate css for styling

## Todo Deployment to Serverless AWS
1. Investigate in Lambda error `Event is undefined`

### Prerequisites on serverless deployment
1. Knowledge on Angular universal(https://github.com/angular/universal)
2. Some knowledge on AWS
3. Setup AWS config in your machine
3. Make sure to have same region in the serverless.yml file

Run ` npm run build:serverless && npm run deploy` to execute deployment via [AWS](https://aws.amazon.com/).


## Author
* **Jose Ricardo Peteros**

## Acknowledgments
* etclp on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
